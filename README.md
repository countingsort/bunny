# bunny
A simple shell script wrapper around multiple package managers

# Supported
-  **Arch Linux/Manjaro/etc** (pacman)
    - trizen
    - yay
    - aurman
- **Void Linux** (xbps)
- **Debian/Ubuntu/etc** (apt-get)
- **OpenBSD** (pkg_add/pkg_info/pkg_delete)
